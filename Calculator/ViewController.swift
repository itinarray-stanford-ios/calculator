//
//  ViewController.swift
//  Calculator
//
//  Created by Kostiantyn Aleksieiev on 2016-08-09.
//  Copyright © 2016 Itinarray. All rights reserved.
//


import UIKit

class ViewController: UIViewController {
    
   

   
    @IBOutlet private weak var display: UILabel!
    
    private var userInTheMiddleOfTyping = false
    
    @IBAction private func touchDigit(sender: UIButton) {
        
        let digit = sender.currentTitle!
       
       
       
        if userInTheMiddleOfTyping {
            
            let textCurentlyInDisplay = display!.text!
            
            if digit == "." && textCurentlyInDisplay.rangeOfString(".") != nil{
               display.text = textCurentlyInDisplay
            } else {
                
                if(textCurentlyInDisplay == "0" && digit != "."){
                    display.text = digit
                }else{
                    display.text = textCurentlyInDisplay + digit
                }

            }
            
        } else {
            
            if digit == "." {
                
                if(display!.text! == "0"){
                  display.text = display!.text! + digit
                }
                
                
            } else  {
                
                display.text = digit
               
                
            }
            
        }
        
            
        
        
        userInTheMiddleOfTyping = true
        
        
    }
    
  
    
    private var displayValue: Double {
       
        get {
            
            return Double(display.text!)!
        }
        
        set {
            
            display.text = String(format:"%g", newValue)
            
            
        }
    }
    
    private var brain = CalculatorBrain()

    @IBAction private func performOperation(sender: UIButton) {
        
        if userInTheMiddleOfTyping{
            brain.setOperand(displayValue)
            userInTheMiddleOfTyping = false
        }
        
       
        
        if let mathematicalSymbol = sender.currentTitle {
            
            brain.performOperation(mathematicalSymbol)
            displayValue = brain.result
            
        }
        
        
    }

}


