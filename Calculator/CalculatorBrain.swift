//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by Kostiantyn Aleksieiev on 2016-08-11.
//  Copyright © 2016 Itinarray. All rights reserved.
//

import Foundation




class CalculatorBrain
    
{
    private var accumulator = 0.0
    
    
    
    func setOperand (operand: Double){
        accumulator = operand
        
    }
    
    private var operations: Dictionary<String, Operation> = [
        "π" : Operation.Constant(M_PI),
        "e": Operation.Constant(M_E),
        "√": Operation.UnaryOperation(sqrt),
        "cos":Operation.UnaryOperation(cos),
        "sin":Operation.UnaryOperation(sin),
        "±": Operation.UnaryOperation({ -$0 }),
        "×" :Operation.BinaryOperation({ $0 * $1 }),
        "+" :Operation.BinaryOperation({ $0 + $1 }),
        "−" :Operation.BinaryOperation({ $0 - $1 }),
        "÷" :Operation.BinaryOperation({ $0 / $1 }),
        "C": Operation.Reset,
        "%": Operation.Percent,
        "=" :Operation.Equals
    ]
    
    private enum Operation {
        case Constant(Double)
        case UnaryOperation((Double)->Double)
        case BinaryOperation((Double, Double)->Double)
        case Equals
        case Reset
        case Percent
    }
    private var pending: PendingBinaryOperationInfo?
    
    private struct PendingBinaryOperationInfo {
        var binaryFunction: (Double, Double)->Double
        var firstOperand: Double
        
    }
    
    func performOperation (symbol:String){
        
        if let operation  = operations[symbol] {
            switch operation {
            case .Constant(let value):
                accumulator = value
            case .UnaryOperation(let function):
                accumulator = function(accumulator)
            case .BinaryOperation(let function):
                executePendingBinaryOperation()
                pending = PendingBinaryOperationInfo(binaryFunction: function
                , firstOperand: accumulator)
            case .Equals:
                executePendingBinaryOperation()
            case .Reset:
            resetAll()
            case .Percent:
            executePercent()
        }
        }
    }
    
   
    
    private func executePendingBinaryOperation(){
        if pending != nil {
           accumulator = pending!.binaryFunction(pending!.firstOperand, accumulator)
            pending = nil
           
        }
        
    }
    
    private func resetAll(){
        pending = nil
        accumulator = 0.0
        
    }
    
    private func executePercent(){
        if pending != nil{
        accumulator = (pending!.firstOperand * accumulator) / 100
        }else{
          accumulator = accumulator / 100
        }
        
    }
    
    var result: Double{
        
        get {
            return accumulator
        }
        
    }
}
